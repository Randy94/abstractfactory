package JasperinVaatteet;

public interface VaateTehdas {
	
	    public Farkut createFarkut();
	    public Tpaita createTpaita();
	    public Lippis createLippis();
	    public Keng�t createKeng�t();
	}
