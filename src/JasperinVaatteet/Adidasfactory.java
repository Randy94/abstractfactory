package JasperinVaatteet;
 
    public class Adidasfactory implements VaateTehdas {
    	
	@Override
    public Farkut createFarkut() {
        return new AdidasFarkut();
    }

    @Override
    public Tpaita createTpaita() {
        return new AdidasTpaita();
    }

    @Override
    public Lippis createLippis() {
        return new AdidasLippis();
    }

    @Override
    public Kengät createKengät() {
        return new AdidasKengät();
    }
}
