package JasperinVaatteet;

public class Jasper {

	    private Farkut farkut;
	    private Keng�t keng�t;
	    private Lippis lippis;
	    private Tpaita tpaita;
	    
	    public Jasper(VaateTehdas tehdas) {
	        farkut = tehdas.createFarkut();
	        keng�t = tehdas.createKeng�t();
	        lippis = tehdas.createLippis();
	        tpaita = tehdas.createTpaita();
	    }
	    
	    @Override
	    public String toString() {
	        String tulostus = "P��ll�ni on: \n";
	        
	        tulostus += farkut + "\n";
	        tulostus += keng�t + "\n";
	        tulostus += lippis + "\n";
	        tulostus += tpaita + "\n";
	        
	        return tulostus;
	    }
}
